<?php

namespace Dottystyle\Laravel\NestedModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class CollectionBuilder
{
    /**
     * @var string
     */
    protected $model;

    /**
     * @var strign
     */
    protected $parentRelation;

    /**
     * @var bool
     */
    protected $shouldSetParentRelation;

    /**
     * @param Illuminate\Database\Eloquent\Model|string $model
     * @param string $parentRelation 
     * @param string $childRelation
     */
    public function __construct($model, $parentRelation, $childRelation, $shouldSetParentRelation = true)
    {
        $this->model = $model instanceof Model ? $model : (new $model);
        $this->parentRelation = $parentRelation;
        $this->childRelation = $childRelation;
        $this->shouldSetParentRelation = $shouldSetParentRelation;

        $this->parentKeyName = (new $model)->{$this->parentRelation}()->getForeignKey();
    }

    /**
     * Get factor catalogue on a nested/tree structure.
     * 
     * @param \Illuminate\Support\Collection $collection (optional)
     * @return \Illuminate\Support\Collection 
     */
    public function build($collection = null)
    {
        if (! $collection) {
            $collection = $this->model->newQuery()->get();
        }

        // Key the model collections by the keys for convenient searching
        $collection = $collection->keyBy($this->model->getKeyName());

        $dictionary = $this->addNestingToDictionary(
            $this->createDictionary($collection), $collection
        );

        $this->applyRelations($dictionary, $collection);

        return $this->removeChildren($collection);
    }

    /**
     * 
     * @return \Illuminate\Support\Collection 
     */
    protected function addNestingToDictionary($dictionary, $collection)
    {
        foreach ($collection as $instance) {
            if (! $this->hasParent($instance)) {
                continue;
            }

            // Push the dictionary entry of the instance to its parent's children
            $parent = $dictionary->get($this->getParentKey($instance));

            $parent->children->push(
                $dictionary->get($instance->getKey())
            );
        }

        return $dictionary;
    }

    /**
     * Check if the model instance has parent or not.
     * 
     * @param \Illuminate\Database\Eloquent\Model $instance
     * @return bool
     */
    protected function hasParent($instance)
    {
        return isset($instance[$this->parentKeyName]);
    }

    /**
     * Get model's parent key
     * 
     * @param \Illuminate\Database\Eloquent\Model $instance
     * @return mixed
     */
    protected function getParentKey($instance)
    {
        return $instance[$this->parentKeyName];
    }

    /**
     * 
     * @param \Illuminate\Support\Collection $collection
     * @param \Illuminate\Support\Collection $dictionary
     * @param \Illuminate\Database\Eloquent\Model $parent
     * @return \Illuminate\Support\Collection
     */
    protected function createDictionary($collection, $parent = null)
    {
        return $collection->map(function ($instance) use ($parent) {
            return $this->createDictionaryEntry($instance, $parent);
        });
    }

    /**
     * 
     * @param \Illuminate\Database\Eloquent\Model $instance
     * @param \Illuminate\Database\Eloquent\Model $parent
     * @return array
     */
    protected function createDictionaryEntry($instance, $parent = null)
    {
        return (object) [
            'instance' => $instance,
            'children' => new Collection()
        ];
    }

    /**
     * Apply relations to the collection models.
     * 
     * @param \Illuminate\Support\Collection $dictionary
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    protected function applyRelations($dictionary, $collection)
    {
        foreach ($dictionary as $entry) {
            $instance = $collection->get($entry->instance->getKey());

            if ($entry->children->isNotEmpty()) {
                $instance->setRelation(
                    $this->childRelation, $this->mapChildrenToRelation($entry->children, $entry->instance)
                );
                
                $this->applyRelations($entry->children, $collection);
            }
        }
    }

    /**
     * Map the dictionary entries to relation value.
     * 
     * @param \Illuminate\Support\Collection $children
     * @param \Illuminate\Database\Eloquent\Model $parent (optional)
     * @return \Illuminate\Support\Collection
     */
    protected function mapChildrenToRelation($children, $parent = null)
    {
        return $children->map(function ($child) use ($parent) {
            if ($parent && $this->shouldSetParentRelation) {
                $child->instance->setRelation($this->parentRelation, $parent);
            }

            return $child->instance;
        });
    }

    /**
     * Remove the children from the collection.
     * 
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    protected function removeChildren($collection)
    {
        // Finally, remove all items that are not parent (children) from the collection
        return $collection->filter(function ($instance) {
            return ! $this->hasParent($instance);
        });
    }
}